﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="21008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2019_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2019\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2019_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2019\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for Leybold CENTER ONE with mains cable, EUR version (230002) and US version (235002).

Note: For binding information refer to the LEYBOLD CENTER ONE operating manual which can be obtained from http://www.leybold.com/vacuumservices.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!*Q(C=\&gt;1^&lt;NN!%)&lt;B4Y;,N,K")_1'=Y)!L.TL#H-&amp;N&lt;&lt;BAE71@K\!+[C*7W/OI-)85/G7?&lt;E;+UQ+K9G!""$8KZ^PF\/0FD3F&gt;HS3&lt;D7?/^Z0(GV]?PFT9HX`?+P_WYR@YT5]TNL\\0RDD8%_&gt;\\_?$$-WTCL@]KPE]&gt;V`$L_@Y_@/:DT=(&lt;3B1^JU;4UJ!@&gt;[&gt;:_X:DE3:\E3:\E32\E12\E12\E1?\E4O\E4O\E4G\E2G\E2G\E2NY;O=B&amp;,H*W*6E]73AJGB2)*E.2]J*Y%E`C34R]6?**0)EH]31?JCDR**\%EXA3$[=J]33?R*.Y%A_FGC2&lt;)]?4?#CPQ".Y!E`A#4QMK=!4!),&amp;AM*"%2A+"I-0A3@Q""Y_+P!%HM!4?!)0QQI]A3@Q"*\!QSFN6[*JJE;/BT*S0)\(]4A?RU.J/2\(YXA=D_.B/4E?R_-AH!7&gt;YB$EH/2-=,YY(M@$GRS0YX%]DM@R-.3OE,?&gt;G424)]&gt;D?!S0Y4%]BI=3-DS'R`!9(M.$72E?QW.Y$)`B93E:(M.D?!S)M3D,SSBGH'B--A,$QV_\7[R&gt;J7A3;[X_.1]XKOI'6.V9KBN'&gt;3/I,L$KQKEOC'KD62OIWBD6$V&lt;^%"61N&lt;#KI'KC^LTO[&amp;P[1/`J'`K;XN&amp;8^/6U[F_?O.`PN&gt;PNN.VO.1S$_L\8:L02?LV7VX6;L6:;,J@(J^5&gt;\8!M0JZ,$XT_^E-P4X@[`(Q`@/W`&gt;)`@8Y?X+&lt;^E0TS8`I6HIWYUTB\T\.&amp;05T8)\1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Configure" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Baud Rate.vi" Type="VI" URL="../Public/Configure/Baud Rate.vi"/>
			<Item Name="Correction Factor.vi" Type="VI" URL="../Public/Configure/Correction Factor.vi"/>
			<Item Name="Filter.vi" Type="VI" URL="../Public/Configure/Filter.vi"/>
			<Item Name="Full Scale Range.vi" Type="VI" URL="../Public/Configure/Full Scale Range.vi"/>
			<Item Name="Offset Correction.vi" Type="VI" URL="../Public/Configure/Offset Correction.vi"/>
			<Item Name="Parameter Setup Lock.vi" Type="VI" URL="../Public/Configure/Parameter Setup Lock.vi"/>
			<Item Name="Setpoint.vi" Type="VI" URL="../Public/Configure/Setpoint.vi"/>
			<Item Name="Torr Lock.vi" Type="VI" URL="../Public/Configure/Torr Lock.vi"/>
			<Item Name="Unit Of Measurement.vi" Type="VI" URL="../Public/Configure/Unit Of Measurement.vi"/>
			<Item Name="Watchdog Control.vi" Type="VI" URL="../Public/Configure/Watchdog Control.vi"/>
			<Item Name="Configure.mnu" Type="Document" URL="../Public/Configure/Configure.mnu"/>
		</Item>
		<Item Name="Action-Status" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Action-Status.mnu" Type="Document" URL="../Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Baud Rate Status.vi" Type="VI" URL="../Public/Action-Status/Baud Rate Status.vi"/>
			<Item Name="Continous Mode.vi" Type="VI" URL="../Public/Action-Status/Continous Mode.vi"/>
			<Item Name="Correction Factor Status.vi" Type="VI" URL="../Public/Action-Status/Correction Factor Status.vi"/>
			<Item Name="Degas Status.vi" Type="VI" URL="../Public/Action-Status/Degas Status.vi"/>
			<Item Name="Degas.vi" Type="VI" URL="../Public/Action-Status/Degas.vi"/>
			<Item Name="Filter Status.vi" Type="VI" URL="../Public/Action-Status/Filter Status.vi"/>
			<Item Name="Full Scale Range Status.vi" Type="VI" URL="../Public/Action-Status/Full Scale Range Status.vi"/>
			<Item Name="High Vacuum Circuit Status.vi" Type="VI" URL="../Public/Action-Status/High Vacuum Circuit Status.vi"/>
			<Item Name="High Vacuum Circuit.vi" Type="VI" URL="../Public/Action-Status/High Vacuum Circuit.vi"/>
			<Item Name="Offset Correction Status.vi" Type="VI" URL="../Public/Action-Status/Offset Correction Status.vi"/>
			<Item Name="Parameter Setup Lock Status.vi" Type="VI" URL="../Public/Action-Status/Parameter Setup Lock Status.vi"/>
			<Item Name="Setpoint Status (Single Channel).vi" Type="VI" URL="../Public/Action-Status/Setpoint Status (Single Channel).vi"/>
			<Item Name="Setpoint Status.vi" Type="VI" URL="../Public/Action-Status/Setpoint Status.vi"/>
			<Item Name="Torr Lock Status.vi" Type="VI" URL="../Public/Action-Status/Torr Lock Status.vi"/>
			<Item Name="Unit Of Measurement Status.vi" Type="VI" URL="../Public/Action-Status/Unit Of Measurement Status.vi"/>
			<Item Name="Watchdog Control Status.vi" Type="VI" URL="../Public/Action-Status/Watchdog Control Status.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Pressure Reading.vi" Type="VI" URL="../Public/Data/Pressure Reading.vi"/>
			<Item Name="Data.mnu" Type="Document" URL="../Public/Data/Data.mnu"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Test" Type="Folder">
				<Item Name="Test AD Converter.vi" Type="VI" URL="../Public/Utility/Test/Test AD Converter.vi"/>
				<Item Name="Test Display.vi" Type="VI" URL="../Public/Utility/Test/Test Display.vi"/>
				<Item Name="Test EEPROM.vi" Type="VI" URL="../Public/Utility/Test/Test EEPROM.vi"/>
				<Item Name="Test EPROM.vi" Type="VI" URL="../Public/Utility/Test/Test EPROM.vi"/>
				<Item Name="Test IO.vi" Type="VI" URL="../Public/Utility/Test/Test IO.vi"/>
				<Item Name="Test Keyboard.vi" Type="VI" URL="../Public/Utility/Test/Test Keyboard.vi"/>
				<Item Name="Test RAM.vi" Type="VI" URL="../Public/Utility/Test/Test RAM.vi"/>
				<Item Name="Test RS232C.vi" Type="VI" URL="../Public/Utility/Test/Test RS232C.vi"/>
				<Item Name="Test.mnu" Type="Document" URL="../Public/Utility/Test/Test.mnu"/>
			</Item>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="ITR Data Output.vi" Type="VI" URL="../Public/Utility/ITR Data Output.vi"/>
			<Item Name="Reset RS232C.vi" Type="VI" URL="../Public/Utility/Reset RS232C.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Save Parameters.vi" Type="VI" URL="../Public/Utility/Save Parameters.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Transmitter Identification.vi" Type="VI" URL="../Public/Utility/Transmitter Identification.vi"/>
			<Item Name="Utility.mnu" Type="Document" URL="../Public/Utility/Utility.mnu"/>
		</Item>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
		<Item Name="dir.mnu" Type="Document" URL="../dir.mnu"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
	</Item>
	<Item Name="Leybold 23xxx2 Readme.html" Type="Document" URL="../Leybold 23xxx2 Readme.html"/>
</Library>
